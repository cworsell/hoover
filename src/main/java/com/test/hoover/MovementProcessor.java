package com.test.hoover;

import com.test.hoover.model.HooverMovementRequest;
import com.test.hoover.model.HooverMovementResponse;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Craig Worsell
 */
public class MovementProcessor {
    
    private static final Logger LOGGER = LogManager.getLogger(MovementProcessor.class);

    private final HooverMovementRequest hoover;
   
    public MovementProcessor(HooverMovementRequest hoover) {
        this.hoover = hoover;
    }
    
    //Main publice method for processing the movement
    public HooverMovementResponse doMovement() {
        HooverMovementResponse movementResponse = new HooverMovementResponse();
        String instructions = hoover.getInstructions();
        for (int i = 0; i < instructions.length(); i++) {
            String direction = instructions.substring(i, i + 1);
            LOGGER.info("Direction: " + direction);
            switch (direction.toUpperCase()) {
                case "N":
                    goNorth();
                    break;
                case "E":
                    goEast();
                    break;
                case "S":
                    goSouth();
                    break;
                case "W":
                    goWest();
                    break;
                default:
            }
            if (isPatchDirty(hoover.getCoords())) {
                hoover.setPatches(cleanPatch(hoover.getCoords()));
                movementResponse.setPatches(movementResponse.getPatches() + 1);
            }
        }
        movementResponse.setCoords(hoover.getCoords());
        return movementResponse;
    }
    
    //Moves the hoover North
    public void goNorth() {
        LOGGER.info("Moving north");
        int current = hoover.getCoords()[1];
        if (isMoveWithinRange(current + 1, 1)) {
            hoover.getCoords()[1] = current + 1;
        }
        logPosition(hoover.getCoords());
    }
    
    //Moves the hoover East
    public void goEast() {
        LOGGER.info("Moving east");
        int current =  hoover.getCoords()[0];
        if (isMoveWithinRange(current + 1, 0)) {
            hoover.getCoords()[0] = current + 1;
        }
        logPosition(hoover.getCoords());
    }
    
    //Moves the hoover South
    public void goSouth() {
        LOGGER.info("Moving south");
        int current = hoover.getCoords()[1];
        if (isMoveWithinRange(current - 1, 1)) {
            hoover.getCoords()[1] = current - 1;
        }
        logPosition(hoover.getCoords());
    }
    
    //Moves the hoover West
    public void goWest() {
        LOGGER.info("Moving west");
        int current =  hoover.getCoords()[0];
        if (isMoveWithinRange(current - 1, 0)) {
            hoover.getCoords()[0] = current - 1;
        }
        logPosition(hoover.getCoords());
    }
    
    //Determines if the movement stays in the range provided
    private boolean isMoveWithinRange(int move, int colRow) {
        if (move > hoover.getRoomSize()[colRow] -1 ||
                move < 0) {
            LOGGER.info("Move not in range");
            return false;
        }
        return true;
    }
    
    //Compares the current patch with the dirty patched
    private boolean isPatchDirty(int[] patch) {
        for (int[] p:hoover.getPatches()) {
            if (Arrays.equals(p, patch)) {
                return true;
            }
        }
        return false;
    }
    
    //Cleans the patch by creating a new array without the dirt patch
    private int[][] cleanPatch(int[] patch) {
        LOGGER.info("Cleaning patch");
        logPosition(patch);
        int[][] cleanedPatches = new int[hoover.getPatches().length - 1][2];
        int[][] dirtyPatches = hoover.getPatches();
        int n = 0;
        for (int i = 0; i < dirtyPatches.length; i++) {
            if (!Arrays.equals(dirtyPatches[i], patch)) {
                cleanedPatches[n++] = dirtyPatches[i];
            }
        }
        return cleanedPatches;
    }
    
    private void logPosition(int[] position) {
        LOGGER.info("Position: [" + position[0] + ", " + position[1] + "]");
    }
}
