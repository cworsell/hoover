package com.test.hoover.controller;

import com.test.hoover.MovementProcessor;
import com.test.hoover.model.HooverMovementRequest;
import com.test.hoover.model.HooverMovementResponse;
import com.test.hoover.repository.HooverRequestRepository;
import com.test.hoover.repository.HooverResponseRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Craig Worsell
 */
@RestController
public class HooverController {
    
    @Autowired
    private HooverRequestRepository reqRepo;
    
    @Autowired
    private HooverResponseRepository resRepo;

    @PostMapping("/hooverMovement")
    public ResponseEntity<HooverMovementResponse> hooverMovementRequest(@RequestBody @Valid HooverMovementRequest req) {
        reqRepo.save(req);
        MovementProcessor processor = new MovementProcessor(req);
        HooverMovementResponse res = processor.doMovement();
        resRepo.save(res);
        return ResponseEntity.ok().body(res);
    }
    
    @GetMapping("/hooverMovement/request")
    public Iterable<HooverMovementRequest> getAllRequests() {
        return reqRepo.findAll();
    }
    
    @GetMapping("/hooverMovement/response")
    public Iterable<HooverMovementResponse> getAllResponses() {
        return resRepo.findAll();
    }
}
