package com.test.hoover.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Craig Worsell
 */
@Entity
public class HooverMovementRequest implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonIgnore
    private long id;
    
    @NotNull(message="The room must be defined")
    @Size(min=2, max=2, message="Please provide X and Y amounts, eg. 5, 5 will have 5 squares down and 5 across")
    private int[] roomSize;
    @NotNull(message="The start position must be provided")
    @Size(min=2, max=2, message="Please provide X and Y as a starting position")
    private int[] coords;
    @NotNull(message="There must be at least one patch")
    private int[][] patches;
    @NotNull(message="The instructions must be provided")
    @Pattern(regexp = "^[NESW]*$", message="Only use N, E, S, W")
    private String instructions;
     
    public HooverMovementRequest() {
    }

    public HooverMovementRequest(int[] roomSize, int[] coords, 
            int[][] patches, String instructions) {
        this.roomSize = roomSize;
        this.coords = coords;
        this.patches = patches;
        this.instructions = instructions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int[] getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(int[] roomSize) {
        this.roomSize = roomSize;
    }

    public int[] getCoords() {
        return coords;
    }

    public void setCoords(int[] coords) {
        this.coords = coords;
    }

    public int[][] getPatches() {
        return patches;
    }

    public void setPatches(int[][] patches) {
        this.patches = patches;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
