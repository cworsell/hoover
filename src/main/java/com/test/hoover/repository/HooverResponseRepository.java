package com.test.hoover.repository;

import com.test.hoover.model.HooverMovementResponse;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Craig Worsell
 */
public interface HooverResponseRepository extends CrudRepository<HooverMovementResponse, Long> {

}
