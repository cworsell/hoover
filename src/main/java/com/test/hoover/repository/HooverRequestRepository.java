package com.test.hoover.repository;

import com.test.hoover.model.HooverMovementRequest;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Craig Worsell
 */
public interface HooverRequestRepository extends CrudRepository<HooverMovementRequest, Long> {
    
}
