package com.test.hoover.builder;

import com.test.hoover.model.HooverMovementRequest;

/**
 *
 * @author Craig Worsell
 */
public class HooverMovementRequestBuilder {
    
    private int[] roomSize = {5, 5};
    private int[] coords = {1, 2};
    private int[][] patches = {{1, 0}, {2, 2}, {2, 3}};
    private String instructions = "NNESEESWNWW";
    
    public HooverMovementRequestBuilder roomSize(int[] roomSize) {
        this.roomSize = roomSize;
        return this;
    }
    
    public HooverMovementRequestBuilder coords(int[] coords) {
        this.coords = coords;
        return this;
    }
    
    public HooverMovementRequestBuilder patches(int[][] patches) {
        this.patches = patches;
        return this;
    }
    
    public HooverMovementRequestBuilder instructions(String instructions) {
        this.instructions = instructions;
        return this;
    }
    
    public HooverMovementRequest build() {
        return new HooverMovementRequest(roomSize, coords, patches, instructions);
    }
}
