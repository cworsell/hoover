package com.test.hoover.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.hoover.HooverApplication;
import com.test.hoover.builder.HooverMovementRequestBuilder;
import com.test.hoover.model.HooverMovementRequest;
import com.test.hoover.model.HooverMovementResponse;
import com.test.hoover.repository.HooverRequestRepository;
import com.test.hoover.repository.HooverResponseRepository;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Craig Worsell
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HooverApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HooverControllerTest {
    
    @Autowired
    private WebApplicationContext wac;
    
    @MockBean
    private HooverRequestRepository reqRepo;
    
    @MockBean
    private HooverResponseRepository resRepo;
    
    private MockMvc mockMvc;
    private final ObjectMapper mapper = new ObjectMapper();
    
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }
    
    @Test
    public void invalidHooverMovementRequestTest() throws Exception {
        ResultActions result = 
                mockMvc.perform(post("/hooverMovement")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void invalidHooverMovementRequestFieldTest() throws Exception {
        HooverMovementRequest req = 
                new HooverMovementRequestBuilder()
                        .coords(null)
                        .instructions(null)
                        .roomSize(null)
                        .patches(null)
                        .build();
        ResultActions result = 
                mockMvc.perform(post("/hooverMovement")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(req)))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void invalidHooverMovementRequestInstructionFieldTest() throws Exception {
        HooverMovementRequest req = 
                new HooverMovementRequestBuilder()
                        .instructions("NEAR")
                        .build();
        ResultActions result = 
                mockMvc.perform(post("/hooverMovement")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(req)))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void validHooverMovementRequestTest() throws Exception {
        HooverMovementRequest req = new HooverMovementRequestBuilder().build();
        ResultActions result = 
                mockMvc.perform(post("/hooverMovement")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(req)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void validHooverMovementResponseTest() throws Exception {
        HooverMovementRequest req = new HooverMovementRequestBuilder().build();
        ResultActions result = 
                mockMvc.perform(post("/hooverMovement")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(req)))
                .andExpect(status().isOk());
        String rString = result.andReturn().getResponse().getContentAsString();
        HooverMovementResponse res = mapper.readValue(rString, HooverMovementResponse.class);
        assertEquals("Response should have 1 patch", 1, res.getPatches());
        assertArrayEquals("Coords should be [1, 3]", new int[]{1, 3}, res.getCoords());
    }
    
    @Test
    public void getSavedRequestsTest() throws Exception {
        Iterable it = Mockito.mock(Iterable.class);
        Mockito.when(reqRepo.findAll()).thenReturn(it);
        ResultActions result = 
                mockMvc.perform(get("/hooverMovement/request"))
                .andExpect(status().isOk());        
    }
    
    @Test
    public void getSavedResponseTest() throws Exception {
        Iterable it = Mockito.mock(Iterable.class);
        Mockito.when(resRepo.findAll()).thenReturn(it);
        ResultActions result = 
                mockMvc.perform(get("/hooverMovement/request"))
                .andExpect(status().isOk());
    }
}
