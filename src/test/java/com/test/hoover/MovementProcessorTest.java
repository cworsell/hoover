package com.test.hoover;

import com.test.hoover.builder.HooverMovementRequestBuilder;
import com.test.hoover.model.HooverMovementResponse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author craig.worsell
 */
@SpringBootTest
public class MovementProcessorTest {
    
    private MovementProcessor processor;
    
    @Before
    public void setup() {
        processor = new MovementProcessor(new HooverMovementRequestBuilder().build());
    }
    
    @Test
    public void canGetInstructionsTest() {
        HooverMovementResponse res = processor.doMovement();
        assertEquals("Patches should be 1", 1, res.getPatches());
        assertArrayEquals("Coords should be [1, 3]", new int[]{1, 3}, res.getCoords());
    }
}
