#Hoover Test

All the source code and tests are included.

Frameworks used: 

* Spring Boot
* Spring Web (MVC, REST)
* Spring Data (JPA)
* H2 (In memory database)
* JUnit
* Mockito

Maven has been used for dependencies and to run the tests and create the build;

Please clone the repository and run the following commands:

1. mvn clean install
2. mvn spring-boot:run

Use a rest client (such as postman) to POST a request to `http://localhost:8080/hooverMovement` with content-type = application/json.
The body should look similar to below, as per the original instructions (https://github.com/lampkicking/java-backend-test):

```{
  "roomSize" : [5, 5],
  "coords" : [1, 2],
  "patches" : [
    [1, 0],
    [2, 2],
    [2, 3]
  ],
  "instructions" : "NNESEESWNWW"
}```

There is validation on the fields but I did not change the default Spring error response which I would usually do but the information is still given in the default message.

There are also endoits to retrieve the saved data, send a GET request to:

* `http://localhost:8080/hooverMovement/request` or
* `http://localhost:8080/hooverMovement/response`